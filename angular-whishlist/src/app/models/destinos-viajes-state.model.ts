import { Injectable } from '@angular/core';
import { Action, UPDATE } from '@ngrx/store';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { Observable, of } from 'rxjs';
import { map } from 'rxjs/operators';
import { DestinoViaje } from './destino-viaje.model';
import { HttpClientModule } from '@angular/common/http';

// ESTADO
export interface DestinosViajesState {
  items: DestinoViaje[];
  loading: boolean;
  favorito: DestinoViaje;
  eliminar: string;
}

export function initializeDestinosViajesState() {
  return {
    items: [],
    loading: false,
    favorito: null,
    eliminar: null
  };
}

// ACCIONES
export enum DestinosViajesActionTypes {
  NUEVO_DESTINO = '[Destinos Viajes] Nuevo',
  ELEGIDO_FAVORITO = '[Destinos Viajes] Favorito',
  VOTE_UP = '[Destinos Viajes] Vote Up',
  VOTE_DOWN = '[Destinos Viajes] Vote Down',
  REINICIAR_VOTOS = '[Destinos Viajes] Reiniciar votos',
  ELIMINAR_DESTINO = '[Destinos Viajes] Eliminar destino',
  INIT_MY_DATA = '[Destinos Viajes] Init my data',
}

export class NuevoDestinoAction implements Action {
  type = DestinosViajesActionTypes.NUEVO_DESTINO;
  constructor(public destino: DestinoViaje) { }
}

export class ElegidoFavoritoAction implements Action {
  type = DestinosViajesActionTypes.ELEGIDO_FAVORITO;
  constructor(public destino: DestinoViaje) { }
}

export class VoteUpAction implements Action {
  type = DestinosViajesActionTypes.VOTE_UP;
  constructor(public destino: DestinoViaje) { }
}

export class VoteDownAction implements Action {
  type = DestinosViajesActionTypes.VOTE_DOWN;
  constructor(public destino: DestinoViaje) { }
}

export class ReiniciarVotosAction implements Action {
  type = DestinosViajesActionTypes.REINICIAR_VOTOS;
  constructor(public destino: DestinoViaje) { }
}

export class EliminarDestinoAction implements Action {
  type = DestinosViajesActionTypes.ELIMINAR_DESTINO;
  constructor(public destino: DestinoViaje) { }
}

export class InitMyDataAction implements Action {
  type = DestinosViajesActionTypes.INIT_MY_DATA;
  constructor(public destino: string[]) { }
}

export type DestinosViajesActions = NuevoDestinoAction | ElegidoFavoritoAction | VoteUpAction | VoteDownAction | ReiniciarVotosAction | EliminarDestinoAction | InitMyDataAction

// REDUCERS
export function reducerDestinosViajes(
  state: DestinosViajesState,
  action: DestinosViajesActions
): DestinosViajesState {
  switch (action.type) {
    case DestinosViajesActionTypes.INIT_MY_DATA: {
      const destinos: string[] = (action as InitMyDataAction).destino;
      return {
          ...state,
          items: destinos.map((d) => new DestinoViaje(d, ''))
        };
    }
    case DestinosViajesActionTypes.NUEVO_DESTINO: {
      return {
        ...state,
        items: [...state.items, (action as NuevoDestinoAction).destino]
      };
    }
    case DestinosViajesActionTypes.ELEGIDO_FAVORITO: {
      state.items.forEach(x => x.setSelected(false));
      const fav: DestinoViaje = (action as ElegidoFavoritoAction).destino;
      fav.setSelected(true);
      return {
        ...state,
        favorito: fav
      };
    }
    case DestinosViajesActionTypes.VOTE_UP: {
      const d: DestinoViaje = (action as VoteUpAction).destino;
      d.voteUp();
      return { ...state };
    }
    case DestinosViajesActionTypes.VOTE_DOWN: {
      const d: DestinoViaje = (action as VoteDownAction).destino;
      d.voteDown();
      return { ...state };
    }
    case DestinosViajesActionTypes.REINICIAR_VOTOS: {
      const d: DestinoViaje = (action as ReiniciarVotosAction).destino;
      d.reiniciarVotos();
      return { ...state };
    }
    case DestinosViajesActionTypes.ELIMINAR_DESTINO: {
      const d: DestinoViaje = (action as EliminarDestinoAction).destino;
      let idToDelete = d.eliminarDestino();
      let count = 0;
      let nombreAEliminar = '';
      state.items.forEach(x => {
        if(x.id == idToDelete){
          console.log("Se ha eliminado ", x.nombre);
          nombreAEliminar = x.nombre;
          state.items.splice(count, 1);
        }
        count++;  
      });
      return { ...state, eliminar: nombreAEliminar };
    }
  }
  return state;
}

// EFFECTS
@Injectable()
export class DestinosViajesEffects {
  @Effect()
  nuevoAgregado$: Observable<Action> = this.actions$.pipe(
    ofType(DestinosViajesActionTypes.NUEVO_DESTINO),
    map((action: NuevoDestinoAction) => new ElegidoFavoritoAction(action.destino))
  );

  constructor(private actions$: Actions) { }
}